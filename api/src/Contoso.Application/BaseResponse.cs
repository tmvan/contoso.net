﻿namespace Contoso.Application
{
    public abstract class BaseResponse
    {
        protected BaseResponse(bool success = true, string message = null)
        {
            IsSuccess = success;
            Message = message;
        }

        public bool IsSuccess { get; private set; }
        public string Message { get; private set; }
    }
}