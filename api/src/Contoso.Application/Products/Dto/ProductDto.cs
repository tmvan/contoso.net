﻿using System;

namespace Contoso.Application.Products.Dto
{
    public class ProductDto
    {
        public string Sku { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsHidden { get; set; }
    }
}