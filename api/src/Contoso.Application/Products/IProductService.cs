﻿using Contoso.Application.Products.Requests;
using Contoso.Application.Products.Responses;

namespace Contoso.Application.Products
{
    public interface IProductService
    {
        CreateProductResponse Create(CreateProductRequest request);

        GetProductsResponse Get(GetProductsRequest request);

        EditProductResponse Edit(EditProductRequest request);

        RemoveProductResponse Remove(RemoveProductRequest request);
    }
}