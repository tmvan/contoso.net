﻿using AutoMapper;
using Contoso.Application.Products.Dto;
using Contoso.Core.Domain.Products;

namespace Contoso.Application.Products
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductDto>()
                .ForMember(dest => dest.Sku, opt => opt.MapFrom(src => src.ProductId.Sku))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Title.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Title.Description))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price.Value))
                .ForMember(dest => dest.IsEnabled, opt => opt.MapFrom(src => !src.State.IsDisabled))
                .ForMember(dest => dest.IsHidden, opt => opt.MapFrom(src => src.State.IsHidden));
        }
    }
}