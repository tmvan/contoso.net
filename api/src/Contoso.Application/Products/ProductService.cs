﻿using AutoMapper;
using Contoso.Application.Products.Dto;
using Contoso.Application.Products.Requests;
using Contoso.Application.Products.Responses;
using Contoso.Core.Domain.Products;
using System;
using System.Linq;

namespace Contoso.Application.Products
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(
            IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public CreateProductResponse Create(CreateProductRequest request)
        {
            var productId = ProductId.Create(Guid.NewGuid(), Guid.NewGuid().ToString("N"));
            var productTitle = ProductTitle.Create(request.Name, request.Description);
            var productPrice = ProductPrice.Create(request.Price);
            var product = Product.Create(productId, productTitle, productPrice);
            var added = _productRepository.Insert(product);
            var dto = Mapper.Map<ProductDto>(added);
            var response = new CreateProductResponse(dto, true);

            return response;
        }

        public EditProductResponse Edit(EditProductRequest request)
        {
            var product = _productRepository.FindBySku(request.Sku);

            if (product == null)
            {
                return new EditProductResponse(false, "Product does not exist");
            }

            var productTitle = ProductTitle.Create(request.Name, request.Description);

            product.ChangeTitle(productTitle);
            _productRepository.Update(product);

            return new EditProductResponse();
        }

        public GetProductsResponse Get(GetProductsRequest request)
        {
            var offset = request.PageSize * (request.PageIndex - 1);
            var limit = request.PageSize;
            var products = _productRepository.Select(offset, limit, out long totalResults);
            var totalPages = (int)Math.Ceiling((double)totalResults / limit);
            var dto = products.Select(Mapper.Map<ProductDto>);

            return new GetProductsResponse(dto, totalPages, totalResults);
        }

        public RemoveProductResponse Remove(RemoveProductRequest request)
        {
            var product = _productRepository.FindBySku(request.Sku);

            if (product == null)
            {
                return new RemoveProductResponse(null, false, "Product does not exist");
            }

            product.Remove();

            var dto = Mapper.Map<ProductDto>(_productRepository.Delete(product));

            return new RemoveProductResponse(dto);
        }
    }
}