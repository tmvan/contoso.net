﻿using System.Collections.Generic;

namespace Contoso.Application.Products.Requests
{
    public class EditProductRequest
    {
        public string Sku { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}