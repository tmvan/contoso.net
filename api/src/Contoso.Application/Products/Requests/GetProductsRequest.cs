﻿namespace Contoso.Application.Products.Requests
{
    public class GetProductsRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}