﻿namespace Contoso.Application.Products.Requests
{
    public class RemoveProductRequest
    {
        public RemoveProductRequest(string sku)
        {
            Sku = sku;
        }

        public string Sku { get; }
    }
}