﻿using Contoso.Application.Products.Dto;

namespace Contoso.Application.Products.Responses
{
    public class CreateProductResponse : BaseResponse
    {
        public CreateProductResponse(ProductDto product, bool success = true, string message = null)
            : base(success, message)
        {
            Product = product;
        }

        public ProductDto Product { get; }
    }
}