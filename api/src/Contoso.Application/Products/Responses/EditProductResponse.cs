﻿namespace Contoso.Application.Products.Responses
{
    public class EditProductResponse : BaseResponse
    {
        public EditProductResponse(bool success = true, string message = null)
            : base(success, message)
        {
        }
    }
}