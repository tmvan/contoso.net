﻿using Contoso.Application.Products.Dto;
using System.Collections.Generic;

namespace Contoso.Application.Products.Responses
{
    public class GetProductsResponse : BaseResponse
    {
        public GetProductsResponse(IEnumerable<ProductDto> products, int totalPages, long totalResults, bool success = true, string message = null)
            : base(success, message)
        {
            Products = products;
            TotalPages = totalPages;
            TotalResults = totalResults;
        }

        public IEnumerable<ProductDto> Products { get; }
        public int TotalPages { get; }
        public long TotalResults { get; }
    }
}