﻿using Contoso.Application.Products.Dto;

namespace Contoso.Application.Products.Responses
{
    public class RemoveProductResponse : BaseResponse
    {
        public RemoveProductResponse(ProductDto product, bool success = true, string message = null) 
            : base(success, message)
        {
            Product = product;
        }

        public ProductDto Product { get; }
    }
}