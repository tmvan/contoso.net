﻿using System;

namespace Contoso.Core.Abstracts.Domain
{
    public abstract class DomainEvent<T> : IDomainEvent where T : IAggregateRoot
    {
        protected DomainEvent()
        {
            TimeStamp = DateTime.UtcNow;
        }

        public DateTime TimeStamp { get; protected set; }

        public Guid AggregateId { get; protected set; }
    }
}