﻿using System.Collections.Generic;

namespace Contoso.Core.Abstracts.Domain
{
    public interface IAggregateRoot : IEntity
    {
        IEnumerable<IDomainEvent> Events { get; }
    }
}