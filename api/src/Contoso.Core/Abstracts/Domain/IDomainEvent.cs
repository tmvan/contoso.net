﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Abstracts.Domain
{
    public interface IDomainEvent
    {
        Guid AggregateId { get; }

        DateTime TimeStamp { get; }
    }
}
