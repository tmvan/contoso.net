﻿using System;

namespace Contoso.Core.Abstracts.Domain
{
    public interface IEntity
    {
        Guid Id { get; }
    }
}