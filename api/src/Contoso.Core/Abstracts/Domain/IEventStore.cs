﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Abstracts.Domain
{
    public interface IEventStore
    {
        void Store(IDomainEvent @event);

        IEnumerable<IDomainEvent> GetAllEvents();

        IEnumerable<IDomainEvent> GetEventsOf(Guid aggregateId);

        IEnumerable<Guid> GetAggregateIds(long offset, int limit, out long totalResults);
    }
}
