﻿using System;
using System.Collections.Generic;

namespace Contoso.Core.Abstracts.Domain
{
    public interface IRepository<T> where T : IAggregateRoot
    {
        T Insert(T item);
        T Update(T item);
        T Delete(T item);
        T FindById(Guid id);
        IEnumerable<T> SelectAll();
        IEnumerable<T> Select(long offset, int limit, out long totalResults);
    }
}