﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.DomainFramework
{
    internal static class Assert
    {
        public static void NotNull(object item, string name = null)
        {
            if (item == null)
            {
                throw new NullReferenceException($"{name ?? nameof(item)} can not be null");
            }
        }

        public static void NotEmpty(string str, string name = null)
        {
            Assert.NotNull(str, name);

            if (str.Length == 0)
            {
                throw new FormatException($"{name ?? nameof(str)} can not be empty");
            }
        }

        public static void True(bool condition, string message = null)
        {
            if (!condition)
            {
                throw new ArgumentException(message ?? "The expression should be true");
            }
        }
    }
}
