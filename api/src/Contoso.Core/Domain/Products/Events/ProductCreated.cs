﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.DomainFramework;
using System;

namespace Contoso.Core.Domain.Products.Events
{
    public class ProductCreated : DomainEvent<Product>
    {
        public ProductId ProductId { get; private set; }

        public ProductTitle ProductTitle { get; private set; }

        public ProductState ProductState { get; private set; }

        public ProductPrice ProductPrice { get; private set; }

        public static ProductCreated Create(Product product)
        {
            Assert.NotNull(product);

            return new ProductCreated
            {
                AggregateId = product.ProductId.Id,
                ProductId = product.ProductId,
                ProductTitle = product.Title,
                ProductState = product.State,
                ProductPrice = product.Price,
            };
        }
    }
}