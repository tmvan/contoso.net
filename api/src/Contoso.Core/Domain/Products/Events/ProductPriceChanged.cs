﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.DomainFramework;
using System;

namespace Contoso.Core.Domain.Products.Events
{
    public class ProductPriceChanged : DomainEvent<Product>
    {
        public ProductPrice NewPrice { get; private set; }

        public static ProductPriceChanged Create(ProductId productId, ProductPrice newPrice)
        {
            Assert.NotNull(productId);
            Assert.NotNull(newPrice);

            return new ProductPriceChanged
            {
                AggregateId = productId.Id,
                NewPrice = newPrice
            };
        }
    }
}