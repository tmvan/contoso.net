﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.DomainFramework;
using System;

namespace Contoso.Core.Domain.Products.Events
{
    public class ProductRemoved : DomainEvent<Product>
    {
        public static ProductRemoved Create(ProductId productId)
        {
            Assert.NotNull(productId);

            return new ProductRemoved
            {
                AggregateId = productId.Id,
            };
        }
    }
}