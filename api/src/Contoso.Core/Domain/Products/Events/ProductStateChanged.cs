﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.DomainFramework;
using System;

namespace Contoso.Core.Domain.Products.Events
{
    public class ProductStateChanged : DomainEvent<Product>
    {
        public ProductState NewState { get; private set; }

        public static ProductStateChanged Create(ProductId productId, ProductState newValue)
        {
            Assert.NotNull(productId);
            Assert.NotNull(newValue);

            return new ProductStateChanged
            {
                AggregateId = productId.Id,
                NewState = newValue
            };
        }
    }
}