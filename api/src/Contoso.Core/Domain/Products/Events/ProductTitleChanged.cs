﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.DomainFramework;
using System;

namespace Contoso.Core.Domain.Products.Events
{
    public class ProductTitleChanged : DomainEvent<Product>
    {
        public ProductTitle NewTitle { get; private set; }

        public static ProductTitleChanged Create(ProductId productId, ProductTitle newTitle)
        {
            Assert.NotNull(productId);
            Assert.NotNull(newTitle);

            return new ProductTitleChanged
            {
                AggregateId = productId.Id,
                NewTitle = newTitle
            };
        }
    }
}