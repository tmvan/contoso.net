﻿using Contoso.Core.Abstracts.Domain;

namespace Contoso.Core.Domain.Products
{
    public interface IProductRepository : IRepository<Product>
    {
        Product FindBySku(string sku);
    }
}