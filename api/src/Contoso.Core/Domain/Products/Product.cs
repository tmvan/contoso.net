﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Products.Events;
using Contoso.Core.DomainFramework;
using System;
using System.Collections.Generic;

namespace Contoso.Core.Domain.Products
{
    public class Product : IAggregateRoot
    {
        private List<IDomainEvent> _events = new List<IDomainEvent>();

        public Guid Id => ProductId.Id;

        public ProductId ProductId { get; private set; }

        public ProductTitle Title { get; private set; }

        public ProductState State { get; private set; }

        public ProductPrice Price { get; private set; }

        public IEnumerable<IDomainEvent> Events => _events;

        public static Product Create(
            ProductId productId,
            ProductTitle productTitle,
            ProductPrice productPrice,
            ProductState productState = null)
        {
            Assert.NotNull(productId, "Product must have id");
            Assert.NotNull(productTitle, "Product must have title");
            Assert.NotNull(productPrice, "Product must have price");

            var product = new Product
            {
                ProductId = productId,
                Title = productTitle,
                Price = productPrice,
                State = productState ?? ProductState.Create()
            };

            product._events.Add(ProductCreated.Create(product));

            return product;
        }
        
        public void ChangeTitle(ProductTitle productTitle)
        {
            Assert.NotNull(productTitle, "Title cannot be null");
            _events.Add(ProductTitleChanged.Create(ProductId, productTitle));
            Title = productTitle;
        }

        public void ChangeState(ProductState productState)
        {
            Assert.NotNull(productState, "State cannot be null");
            _events.Add(ProductStateChanged.Create(ProductId, productState));
            State = productState;
        }

        public void ChangePrice(ProductPrice productPrice)
        {
            Assert.NotNull(productPrice, "Value cannot be null");
            Assert.True(productPrice.Value > 0, "Product price must be greater than zero");
            _events.Add(ProductPriceChanged.Create(ProductId, productPrice));
            Price = productPrice;
        }

        public void Remove()
        {
            _events.Add(ProductRemoved.Create(ProductId));
        }
    }
}