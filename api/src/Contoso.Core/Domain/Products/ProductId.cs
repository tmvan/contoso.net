﻿using Contoso.Core.Abstracts.Domain;
using System;

namespace Contoso.Core.Domain.Products
{
    public class ProductId : ValueObject
    {
        public Guid Id { get; private set; }

        public string Sku { get; private set; }

        public static ProductId Create(Guid id, string sku)
            => new ProductId { Id = id, Sku = sku };
    }
}