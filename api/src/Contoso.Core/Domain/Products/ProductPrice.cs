﻿using Contoso.Core.Abstracts.Domain;

namespace Contoso.Core.Domain.Products
{
    public class ProductPrice : ValueObject
    {
        public decimal Value { get; private set; }

        public static ProductPrice Create(decimal value)
        {
            return new ProductPrice { Value = value };
        }
    }
}