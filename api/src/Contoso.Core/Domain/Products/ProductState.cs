﻿using Contoso.Core.Abstracts.Domain;

namespace Contoso.Core.Domain.Products
{
    public class ProductState : ValueObject
    {
        public bool IsDisabled { get; private set; }

        public bool IsHidden { get; private set; }

        public static ProductState Create(bool disabled = false, bool hide = false)
            => new ProductState { IsDisabled = disabled, IsHidden = hide };
    }
}