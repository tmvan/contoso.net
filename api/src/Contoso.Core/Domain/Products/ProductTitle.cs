﻿using Contoso.Core.Abstracts.Domain;

namespace Contoso.Core.Domain.Products
{
    public class ProductTitle : ValueObject
    {
        public string Name { get; private set; }

        public string Description { get; private set; }

        public static ProductTitle Create(string name, string description)
        {
            return new ProductTitle
            {
                Name = name,
                Description = description
            };
        }
    }
}