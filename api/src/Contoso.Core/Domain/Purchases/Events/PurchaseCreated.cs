﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.DomainFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Domain.Purchases.Events
{
    public class PurchaseCreated : DomainEvent<Purchase>
    {
        public PurchaseId PurchaseId { get; private set; }

        public PurchasePerson Customer { get; private set; }

        public DateTime PurchaseTime { get; private set; }

        public static PurchaseCreated Create(
            PurchaseId purchaseId,
            PurchasePerson customer,
            DateTime purchaseTime)
        {
            Assert.NotNull(purchaseId, "Purchase must have value");
            Assert.NotNull(customer, "Purchase must have customer");

            return new PurchaseCreated
            {
                AggregateId = purchaseId.Id,
                PurchaseId = purchaseId,
                Customer = customer,
                PurchaseTime = purchaseTime
            };
        }
    }
}
