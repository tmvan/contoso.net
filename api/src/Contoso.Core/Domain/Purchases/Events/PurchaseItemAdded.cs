﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.DomainFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Domain.Purchases.Events
{
    public class PurchaseItemAdded : DomainEvent<Purchase>
    {
        public PurchaseItem Item { get; private set; }

        public static PurchaseItemAdded Create(PurchaseId purchaseId, PurchaseItem purchaseItem)
        {
            Assert.NotNull(purchaseId, "PurchaseId must not be null");
            Assert.NotNull(purchaseItem, "purchaseItem must not be null");

            return new PurchaseItemAdded
            {
                AggregateId = purchaseId.Id,
                Item = purchaseItem
            };
        }
    }
}
