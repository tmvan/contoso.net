﻿using Contoso.Core.Abstracts.Domain;

namespace Contoso.Core.Domain.Purchases
{
    public interface IPurchaseRepository : IRepository<Purchase>
    {
        Purchase FindByCode(string code);
    }
}