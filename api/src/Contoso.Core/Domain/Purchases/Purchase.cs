﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Products;
using Contoso.Core.Domain.Purchases.Events;
using Contoso.Core.DomainFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Domain.Purchases
{
    public class Purchase : IAggregateRoot
    {
        private IList<PurchaseItem> _items = new List<PurchaseItem>();

        private IList<IDomainEvent> _events = new List<IDomainEvent>();

        public Guid Id => PurchaseId.Id;

        public PurchaseId PurchaseId { get; private set; }

        public PurchasePerson Customer { get; private set; }

        public DateTime PurchaseTime { get; private set; }

        public decimal Total => _items.Sum(purchaseItem => purchaseItem.Total);

        public IEnumerable<PurchaseItem> Items => _items;

        public IEnumerable<IDomainEvent> Events => _events;

        public static Purchase Create(
            PurchaseId purchaseId,
            PurchasePerson customer,
            DateTime purchaseTime,
            IEnumerable<PurchaseQuantity> lines)
        {
            Assert.True(lines?.Count() > 0, "Purchase must have item");

            var purchase = new Purchase
            {
                PurchaseId = purchaseId,
                Customer = customer,
                PurchaseTime = purchaseTime
            };

            foreach(var line in lines)
            {
                var purchaseItem = PurchaseItem.Create(Guid.NewGuid(), line.Product, line.Quantity);
                purchase.AddItem(purchaseItem);
            }

            purchase._events.Add(PurchaseCreated.Create(purchaseId, customer, purchaseTime));

            return purchase;
        }

        private void AddItem(PurchaseItem item)
        {
            Assert.NotNull(item, "Purchase item must not be null");
            Assert.True(!_items.Any(x => x.ProductId == item.ProductId), $"Product {item.ProductTitle.Name} is already added");

            _items.Add(item);
            _events.Add(PurchaseItemAdded.Create(PurchaseId, item));
        }
    }
}
