﻿using Contoso.Core.Abstracts.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Domain.Purchases
{
    public class PurchaseId : ValueObject
    {
        public Guid Id { get; private set; }

        public string Code { get; private set; }

        public static PurchaseId Create(Guid id, string code)
            => new PurchaseId { Id = id, Code = code };
    }
}
