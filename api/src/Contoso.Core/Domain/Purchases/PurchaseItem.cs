﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Domain.Purchases
{
    public class PurchaseItem : IEntity
    {
        public Guid Id { get; private set; }

        public ProductId ProductId { get; private set; }

        public ProductTitle ProductTitle { get; private set; }

        public ProductPrice ProductPrice { get; private set; }

        public int Quantity { get; private set; }

        public decimal Total => ProductPrice.Value * Quantity;

        public static PurchaseItem Create(Guid id, Product product, int quantity)
             => new PurchaseItem
             {
                 Id = id,
                 ProductId = product.ProductId,
                 ProductTitle = product.Title,
                 ProductPrice = product.Price,
                 Quantity = quantity
             };
    }
}
