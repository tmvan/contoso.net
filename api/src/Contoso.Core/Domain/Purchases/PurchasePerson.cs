﻿using Contoso.Core.Abstracts.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Domain.Purchases
{
    public class PurchasePerson : ValueObject
    {
        public string FullName { get; private set; }

        public string Phone { get; private set; }

        public string Email { get; private set; }

        public static PurchasePerson Create(string fullName, string phone, string email)
            => new PurchasePerson { FullName = fullName, Phone = phone, Email = email };
    }
}
