﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Domain.Purchases
{
    public class PurchaseQuantity : ValueObject
    {
        public Product Product { get; private set; }

        public int Quantity { get; private set; }

        public static PurchaseQuantity Create(Product product, int quantity)
            => new PurchaseQuantity { Product = product, Quantity = quantity };
    }
}
