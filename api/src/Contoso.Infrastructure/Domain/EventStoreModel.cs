﻿using LiteDB;
using System;

namespace Contoso.Infrastructure.Domain
{
    public class EventStoreModel
    {
        public ObjectId Id { get; set; }

        public Guid AggregateId { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Event { get; set; }

        public string Data { get; set; }
    }
}