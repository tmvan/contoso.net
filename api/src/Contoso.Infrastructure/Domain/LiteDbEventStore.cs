﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Infrastructure.Json;
using LiteDB;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Web;

namespace Contoso.Infrastructure.Domain
{
    public class LiteDbEventStore : IEventStore
    {
        #region static

        private static readonly string CacheKey = typeof(LiteDbEventStore).FullName;

        private static LiteDatabase CreateLiteDatabase()
        {
            var path = HttpContext.Current.Server.MapPath("~/App_Data");
            Directory.CreateDirectory(path);
            return new LiteDatabase(Path.Combine(path, "Events.db"));
        }

        #endregion static

        private readonly LiteDatabase _db;

        public LiteDbEventStore()
        {
            var db = MemoryCache.Default.Get(CacheKey) as LiteDatabase;

            if (db == null)
            {
                db = CreateLiteDatabase();
                MemoryCache.Default.Add(CacheKey, db, new CacheItemPolicy
                {
                    SlidingExpiration = TimeSpan.FromMinutes(3),
                    RemovedCallback = args =>
                    {
                        try
                        {
                            (args.CacheItem.Value as LiteDatabase)?.Dispose();
                        }
                        catch { }
                    }
                });
            }

            // TODO: Need to thread safe

            _db = db;
        }

        public IEnumerable<IDomainEvent> GetAllEvents()
        {
            var collection = _db.GetCollection<EventStoreModel>();
            var events = collection.FindAll();
            return ConvertEventStoreModelToDomainEvent(events);
        }

        public void Store(IDomainEvent @event)
        {
            var collection = _db.GetCollection<EventStoreModel>();
            var type = @event.GetType();
            var assemblyName = type.Assembly.GetName().Name;

            collection.Insert(new EventStoreModel
            {
                AggregateId = @event.AggregateId,
                TimeStamp = @event.TimeStamp,
                Event = $"{type.FullName},{assemblyName}",
                Data = JsonConvert.SerializeObject(@event)
            });
        }

        public IEnumerable<IDomainEvent> GetEventsOf(Guid aggregateId)
        {
            var collection = _db.GetCollection<EventStoreModel>();
            var events = collection.Find(x => x.AggregateId == aggregateId);
            return ConvertEventStoreModelToDomainEvent(events);
        }

        private IEnumerable<IDomainEvent> ConvertEventStoreModelToDomainEvent(IEnumerable<EventStoreModel> items)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new PrivateMemberContractResolver()
            };

            return items.Select(item =>
            {
                var type = Type.GetType(item.Event);
                return (IDomainEvent)JsonConvert.DeserializeObject(item.Data, type, settings);
            });
        }

        public IEnumerable<Guid> GetAggregateIds(long offset, int limit, out long totalResults)
        {
            var collection = _db.GetCollection<EventStoreModel>();
            var aggregates = collection.FindAll().Select(x => x.AggregateId).Distinct();

            totalResults = aggregates.LongCount();

            var skipCount = 0L;

            while (skipCount + int.MaxValue < offset)
            {
                aggregates.Skip(int.MaxValue);
                skipCount += int.MaxValue;
            }

            aggregates = aggregates.Skip((int)(offset - skipCount));
            return aggregates.Take(limit);
        }
    }
}