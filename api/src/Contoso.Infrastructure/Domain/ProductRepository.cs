﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Products;
using Contoso.Core.Domain.Products.Events;
using System;
using System.Collections.Generic;
using Fasterflect;
using System.Linq;

namespace Contoso.Infrastructure.Domain
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IEventStore eventStore)
            : base(eventStore)
        {
        }

        public Product FindBySku(string sku)
        {
            return SelectAll().SingleOrDefault(x => x.ProductId.Sku == sku);
        }

        protected override Product Restore(IEnumerable<IDomainEvent> events)
        {
            var queue = new Queue<IDomainEvent>(events);

            if(queue.Count < 1)
            {
                return null;
            }

            var firstEvent = queue.Dequeue();
            var product = new Product();

            if (firstEvent is ProductCreated)
            {
                var @event = (ProductCreated)firstEvent;
                product.SetPropertyValue(nameof(product.ProductId), @event.ProductId, Flags.InstanceAnyVisibility);
                product.SetPropertyValue(nameof(product.Title), @event.ProductTitle, Flags.InstanceAnyVisibility);
                product.SetPropertyValue(nameof(product.Price), @event.ProductPrice, Flags.InstanceAnyVisibility);
                product.SetPropertyValue(nameof(product.State), @event.ProductState, Flags.InstanceAnyVisibility);
            }
            else
            {
                throw new InvalidOperationException($"First event must be {typeof(ProductCreated)}");
            }

            while (queue.Count > 0)
            {
                var currentEvent = queue.Dequeue();

                if (currentEvent is ProductTitleChanged)
                {
                    var @event = (ProductTitleChanged)currentEvent;
                    product.SetPropertyValue(nameof(product.Title), @event.NewTitle, Flags.InstanceAnyVisibility);
                }
                else if (currentEvent is ProductPriceChanged)
                {
                    var @event = (ProductPriceChanged)currentEvent;
                    product.SetPropertyValue(nameof(product.Price), @event.NewPrice, Flags.InstanceAnyVisibility);
                }
                else if (currentEvent is ProductStateChanged)
                {
                    var @event = (ProductStateChanged)currentEvent;
                    product.SetPropertyValue(nameof(product.State), @event.NewState, Flags.InstanceAnyVisibility);
                }
                else if (currentEvent is ProductRemoved)
                {
                    product = null;
                    break;
                }
                else
                {
                    throw new InvalidOperationException($"Unknown event: {currentEvent.GetType()}");
                }
            }

            return product;
        }
    }
}