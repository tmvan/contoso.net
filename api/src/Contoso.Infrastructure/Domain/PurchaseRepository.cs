﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Purchases;
using Contoso.Core.Domain.Purchases.Events;
using Fasterflect;
using System;
using System.Collections.Generic;

namespace Contoso.Infrastructure.Domain
{
    public class PurchaseRepository : RepositoryBase<Purchase>, IPurchaseRepository
    {
        public PurchaseRepository(IEventStore eventStore)
            : base(eventStore)
        {
        }

        public Purchase FindByCode(string code)
        {
            throw new NotImplementedException();
        }

        protected override Purchase Restore(IEnumerable<IDomainEvent> events)
        {
            var queue = new Queue<IDomainEvent>(events);

            if (queue.Count < 1)
            {
                return null;
            }

            var firstEvent = queue.Dequeue();
            var purchase = new Purchase();

            if (firstEvent is PurchaseCreated)
            {
                var @event = (PurchaseCreated)firstEvent;
                purchase.SetPropertyValue(nameof(purchase.PurchaseId), @event.PurchaseId, Flags.InstanceAnyVisibility);
                purchase.SetPropertyValue(nameof(purchase.Customer), @event.Customer, Flags.InstanceAnyVisibility);
                purchase.SetPropertyValue(nameof(purchase.PurchaseTime), @event.PurchaseTime, Flags.InstanceAnyVisibility);
            }
            else
            {
                throw new InvalidOperationException($"First event must be {typeof(PurchaseCreated)}");
            }

            while (queue.Count > 0)
            {
                var currentEvent = queue.Dequeue();

                if (currentEvent is PurchaseItemAdded)
                {
                    var @event = (PurchaseItemAdded)currentEvent;

                    if (@event.AggregateId != purchase.Id)
                    {
                        throw new InvalidOperationException("Invalid purchase item");
                    }

                    purchase.CallMethod("AddItem", Flags.InstanceAnyVisibility, @event.Item);
                }
                else
                {
                    throw new InvalidOperationException($"Unknown event: {currentEvent.GetType()}");
                }
            }

            return purchase;
        }
    }
}