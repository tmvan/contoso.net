﻿using Contoso.Core.Abstracts.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Contoso.Infrastructure.Domain
{
    public abstract class RepositoryBase<T> : IRepository<T>
        where T : IAggregateRoot
    {
        private readonly IEventStore _eventStore;

        public RepositoryBase(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        protected abstract T Restore(IEnumerable<IDomainEvent> events);

        private void StoreEvent(T item)
        {
            foreach (var @event in item.Events)
            {
                _eventStore.Store(@event);
            }
        }

        public T Insert(T item)
        {
            StoreEvent(item);
            return item;
        }

        public T FindById(Guid id)
        {
            var events = _eventStore.GetEventsOf(id);
            return Restore(events);
        }

        public IEnumerable<T> Select(long offset, int limit, out long totalResults)
        {
            var aggregates = _eventStore.GetAggregateIds(offset, limit, out totalResults);
            return aggregates.Select(FindById).Where(x => x != null);
        }

        public IEnumerable<T> SelectAll()
        {
            var events = _eventStore.GetAllEvents();
            return events
                .GroupBy(
                    @event => @event.AggregateId,
                    @event => @event,
                    (aggregateId, groupEvents) => Restore(groupEvents))
                .Where(x => x != null);
        }

        public T Delete(T item)
        {
            StoreEvent(item);
            return item;
        }

        public T Update(T item)
        {
            StoreEvent(item);
            return item;
        }
    }
}