﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Infrastructure.Services.Mailing
{
    public interface IMailService
    {
        Task SendAsync(string email, string title, string body);
    }
}
