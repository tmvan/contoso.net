﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Infrastructure.Services.Mailing
{
    public class MailService : IMailService
    {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Task SendAsync(string email, string title, string body)
        {
            Log.Info($"Sent mail to {email}:\n - Title: {title}\n - Body: {body}");
            return Task.CompletedTask;
        }
    }
}
