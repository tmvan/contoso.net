﻿using AutoMapper;
using Contoso.Application.Products.Dto;
using System.Linq;

namespace Contoso.Presentation.WebApi
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            var assemblies = new[] {
                typeof(ProductDto)
            }.Select(x => x.Assembly);

            Mapper.Initialize(cfg => cfg.AddProfiles(assemblies));
        }
    }
}