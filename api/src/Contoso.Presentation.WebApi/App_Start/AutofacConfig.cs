﻿using Autofac;
using Contoso.Application.Products;
using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Products;
using Contoso.Infrastructure.Domain;

namespace Contoso.Presentation.WebApi
{
    public class AutofacConfig
    {
        public static void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<ProductService>().As<IProductService>();
            builder.RegisterType<ProductRepository>().As<IProductRepository>();
            builder.RegisterType<LiteDbEventStore>().As<IEventStore>();
        }
    }
}