﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Contoso.Presentation.WebApi.Controllers
{
    [RoutePrefix("api/v1/codes")]
    public class CodesController : ApiController
    {
        [Route("204")]
        [AcceptVerbs("GET")]
        public void Generate204()
        { }

        [Route("200")]
        [AcceptVerbs("GET", "POST", "PUT", "PATCH")]
        public IHttpActionResult Generate200()
        {
            return this.Ok();
        }
    }
}
