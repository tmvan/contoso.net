﻿using Contoso.Application.Products;
using Contoso.Application.Products.Requests;
using Contoso.Application.Products.Responses;
using System.Web.Http;

namespace Contoso.Presentation.WebApi.Controllers
{
    [RoutePrefix("api/v1/products")]
    public class ProductsController : ApiController
    {
        private readonly IProductService _productService;

        public ProductsController(
            IProductService productService)
        {
            _productService = productService;
        }

        [Route("")]
        [AcceptVerbs("GET")]
        public GetProductsResponse Get([FromUri] GetProductsRequest request)
        {
            return _productService.Get(request);
        }

        [Route("")]
        [AcceptVerbs("POST")]
        public CreateProductResponse Create([FromBody] CreateProductRequest request)
        {
            return _productService.Create(request);
        }

        [Route("")]
        [AcceptVerbs("PUT")]
        public EditProductResponse Edit([FromBody] EditProductRequest request)
        {
            return _productService.Edit(request);
        }

        [Route("")]
        [AcceptVerbs("DELETE")]
        public RemoveProductResponse Remove([FromBody] RemoveProductRequest request)
        {
            return _productService.Remove(request);
        }
    }
}