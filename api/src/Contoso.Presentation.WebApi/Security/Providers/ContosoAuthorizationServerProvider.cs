﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Presentation.WebApi.Security.Providers
{
    public class ContosoAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            var clientId = string.Empty;
            var clientSecret = string.Empty;
            
            if(!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }
            
            // client_credentials
            if((clientId != "client" && clientId != "develop") || clientSecret != "secret")
            {
                context.SetError("invalid_client", "client is invalid");
                return Task.CompletedTask;
            }

            context.OwinContext.Set("as:clientAllowOrigin", clientId == "develop" ? "localhost" : "*");
            context.OwinContext.Set("as:clientRefreshTokenLifeTime", 86400);
            context.Validated();

            return Task.CompletedTask;
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var allowOrigin = context.OwinContext.Get<string>("as:clientAllowOrigin");

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowOrigin });

            if (string.IsNullOrWhiteSpace(context.UserName) || string.IsNullOrWhiteSpace(context.Password))
            {
                context.SetError("invalid_grant", "username or password is invalid");
            }
            else
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaim(new Claim("sub", context.UserName));
                identity.AddClaim(new Claim("role", "user"));

                var properties = new AuthenticationProperties(new Dictionary<string, string>
                {
                    ["as:clientId"] = context.ClientId ?? string.Empty,
                    ["userName"] = context.UserName
                });
                var ticket = new AuthenticationTicket(identity, properties);

                context.Validated(ticket);
            }

            return Task.CompletedTask;
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach(var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return base.TokenEndpoint(context);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClientId = context.Ticket.Properties.Dictionary["as:clientId"];
            var currentClient = context.ClientId;

            if(originalClientId != context.ClientId)
            {
                context.SetError("invalid_client", "client is invalid");
                return Task.CompletedTask;
            }

            context.Validated(context.Ticket);

            return Task.CompletedTask;
        }
    }
}
