﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Presentation.WebApi.Security.Providers
{
    public class ContosoRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private static readonly Dictionary<string, string> RefreshTokens = new Dictionary<string, string>();

        private static string Hash(string input)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientId = context.Ticket.Properties.Dictionary["as:clientId"];

            if (string.IsNullOrWhiteSpace(clientId))
            {
                return Task.CompletedTask;
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");
            var refreshTokenLifeTime = context.OwinContext.Get<int>("as:clientRefreshTokenLifeTime");
            var hashedRefreshTokenId = Hash(refreshTokenId);

            context.Ticket.Properties.IssuedUtc = DateTime.UtcNow;
            context.Ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddSeconds(refreshTokenLifeTime);
            RefreshTokens.Add(hashedRefreshTokenId, context.SerializeTicket());
            context.SetToken(refreshTokenId);

            return Task.CompletedTask;
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowOrigin = context.OwinContext.Get<string>("as:clientAllowOrigin");
            var hashedRefreshTokenId = Hash(context.Token);

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowOrigin });

            if (RefreshTokens.ContainsKey(hashedRefreshTokenId))
            {
                context.DeserializeTicket(RefreshTokens[hashedRefreshTokenId]);
                RefreshTokens.Remove(hashedRefreshTokenId);
            }
            
            return Task.CompletedTask;
        }
    }
}
