﻿using Contoso.Core.Domain.Products;
using Contoso.Core.Domain.Products.Events;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Tests.Domain
{
    [TestClass]
    public class ProductTests
    {
        [TestMethod]
        public void When_CreateProduct_Expect_ProductCreated()
        {
            var productId = Guid.NewGuid();
            var sku = "PRO001";
            var product = Product.Create(
                ProductId.Create(productId, sku),
                ProductTitle.Create("Product 1", "Description"),
                ProductPrice.Create(10000));

            product.Should().NotBeNull("Product object should not be null");
            product.Id.Should().Be(productId, "Product id should be " + productId);
            product.ProductId.Should().NotBeNull("ProductId object should not be null");
            product.ProductId.Id.Should().Be(productId, "ProductId.Id should be " + productId);
            product.ProductId.Sku.Should().NotBeNull("ProductId.Sku should not be null");
            product.ProductId.Sku.Should().Be(sku, "ProductId.Sku should be " + sku);
            product.Title.Should().NotBeNull("Product.Title property should not be null");
            product.Title.Name.Should().NotBeNull("Product.Title.Name should not be null");
            product.Title.Name.Should().Be("Product 1", "Product.Title.Name should be 'Product 1'" );
            product.Title.Description.Should().NotBeNull("Product.Title.Description should not be null");
            product.Title.Description.Should().Be("Description", "Product.Title.Description should be 'Description'");
            product.Price.Should().NotBeNull("Product.Price property should not be null");
            product.Price.Value.Should().Be(10000, "Product.Price should be 10000");
            product.State.Should().NotBeNull("Product.State property should not be null");
            product.State.IsDisabled.Should().Be(false, "Because uninitilized value of IsDisabled should be False");
            product.State.IsHidden.Should().Be(false, "Because uninitilized value of IsHidden should be False");
        }

        [TestMethod]
        public void When_CreateProduct_Expect_EventsCreated()
        {
            var productId = Guid.NewGuid();
            var sku = "PRO001";
            var now = DateTime.Now;
            var product = Product.Create(
                ProductId.Create(productId, sku),
                ProductTitle.Create("Product 1", "Description"),
                ProductPrice.Create(10000));

            product.Should().NotBeNull("Product object should not be null");
            product.Events.Should().NotBeNull("Product.Events property should not be null");
            product.Events.Count().Should().Be(1, "Product have just created should have 1 event");

            var firstEvent = product.Events.First();
            firstEvent.Should().BeOfType<ProductCreated>("Product have just created should have only ProductCreated event");
            firstEvent.AggregateId.Should().Be(productId, "Event aggregateId should be " + productId);

            var productCreatedEvent = firstEvent.As<ProductCreated>();
            productCreatedEvent.ProductId.Should().NotBeNull("ProductCreated.ProductId should not be null");
            productCreatedEvent.ProductId.Id.Should().Be(productId, "ProductCreated.ProductId.Id should be " + productId);
            productCreatedEvent.ProductId.Sku.Should().Be(sku, "ProductCreated.ProductId.Sku should be " + sku);
            productCreatedEvent.ProductTitle.Should().NotBeNull("ProductCreated.ProductTitle should not be null");
            productCreatedEvent.ProductTitle.Name.Should().NotBeNull("ProductCreated.ProductTitle.Name should not be null");
            productCreatedEvent.ProductTitle.Name.Should().Be("Product 1", "ProductCreated.ProductTitle.Name should be 'Product 1'");
            productCreatedEvent.ProductTitle.Description.Should().NotBeNull("ProductCreated.ProductTitle.Description should not be null");
            productCreatedEvent.ProductTitle.Description.Should().Be("Description", "ProductCreated.ProductTitle.Description should be 'Description'");
            productCreatedEvent.ProductPrice.Should().NotBeNull("ProductCreated.ProductPrice should not be null");
            productCreatedEvent.ProductPrice.Value.Should().Be(10000, "ProductCreated.ProductPrice.Value should be 10000");
            productCreatedEvent.ProductState.Should().NotBeNull("ProductCreated.ProductState should not be null");
            productCreatedEvent.ProductState.IsDisabled.Should().Be(false, "ProductCreated.ProductState.IsDisabled should not be false as default");
            productCreatedEvent.ProductState.IsHidden.Should().Be(false, "ProductCreated.ProductState.IsHidden should not be false as default");
        }

        [TestMethod]
        public void When_ChangeProductTitle_Expect_TitleChanged()
        {
            var productId = Guid.NewGuid();
            var sku = "PRO001";
            var product = Product.Create(
                ProductId.Create(productId, sku),
                ProductTitle.Create("Product 1", "Description 1"),
                ProductPrice.Create(10000));
            product.ChangeTitle(ProductTitle.Create("Product 2", "Description 2"));

            Assert.AreNotEqual("Product 1", product.Title.Name);
            Assert.AreNotEqual("Description 1", product.Title.Description);
            Assert.AreEqual("Product 2", product.Title.Name);
            Assert.AreEqual("Description 2", product.Title.Description);
        }

        [TestMethod]
        public void When_ChangeProductTitle_Expect_EventCreated()
        {
            var productId = Guid.NewGuid();
            var sku = "PRO001";
            var product = Product.Create(
                ProductId.Create(productId, sku),
                ProductTitle.Create("Product 1", "Description 1"),
                ProductPrice.Create(10000));
            product.ChangeTitle(ProductTitle.Create("Product 2", "Description 2"));

            Assert.IsNotNull(product.Events, "events must not be null");
            Assert.AreEqual(2, product.Events.Count(), "events quantity must be 2");
            Assert.IsInstanceOfType(product.Events.ElementAt(0), typeof(ProductCreated), "first event must be ProductCreated");
            Assert.IsInstanceOfType(product.Events.ElementAt(1), typeof(ProductTitleChanged), "second event must be ProductTitleChanged");

            foreach (var @event in product.Events)
            {
                Assert.AreEqual(productId, @event.AggregateId, "event aggregate id is wrong");
            }

            Assert.AreEqual("Product 2", (product.Events.ElementAt(1) as ProductTitleChanged).NewTitle.Name, "event title name is incorrect");
            Assert.AreEqual("Description 2", (product.Events.ElementAt(1) as ProductTitleChanged).NewTitle.Description, "event description is incorrect");
        }
    }
}
