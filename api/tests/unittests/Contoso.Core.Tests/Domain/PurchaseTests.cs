﻿using Contoso.Core.Domain.Products;
using Contoso.Core.Domain.Purchases;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Core.Tests.Domain
{
    [TestClass]
    public class PurchaseTests
    {
        [TestMethod]
        public void When_CreatePurchase_Expect_PurchaseCreated()
        {
            var purchaseId = Guid.NewGuid();
            var purchaseTime = DateTime.UtcNow;
            var productId = Guid.NewGuid();
            var product = Product.Create(
                ProductId.Create(productId, "P0001"),
                ProductTitle.Create("Test product", "Test description"),
                ProductPrice.Create(100000M),
                ProductState.Create());
            var purchaseQuantity = PurchaseQuantity.Create(
                product,
                10);
            var purchase = Purchase.Create(
                PurchaseId.Create(purchaseId, "PC0001"),
                PurchasePerson.Create("John Doe", "0123456789", "test@example.com"),
                purchaseTime,
                new[] { purchaseQuantity });

            purchase.Should().NotBeNull("Purchase object should not be null");
            purchase.Id.Should().Be(purchaseId, "purchase id should be " + purchaseId);
            purchase.PurchaseId.Should().NotBeNull("purchaseId object should not be null");
            purchase.PurchaseId.Id.Should().Be(purchaseId, "PurchaseId.Id should be " + purchaseId);
            purchase.PurchaseId.Code.Should().Be("PC0001", "PurchaseId.Code should be PC0001");
            purchase.Customer.Should().NotBeNull("Customer object should not be null");
            purchase.Customer.FullName.Should().NotBeNull();
            purchase.Customer.Phone.Should().NotBeNull();
            purchase.Customer.Email.Should().NotBeNull();
        }

        [TestMethod]
        public void When_CreatePurchase_Expect_EventsCreated()
        {
        }
    }
}
