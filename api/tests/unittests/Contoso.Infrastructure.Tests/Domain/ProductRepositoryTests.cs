﻿using Contoso.Core.Abstracts.Domain;
using Contoso.Core.Domain.Products;
using Contoso.Infrastructure.Domain;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contoso.Infrastructure.Tests.Domain
{
    [TestClass]
    public class ProductRepositoryTests
    {
        public IProductRepository Repository { get; }

        private Guid ValidProductId { get; }

        public ProductRepositoryTests()
        {
            ValidProductId = Guid.NewGuid();

            var product = Product.Create(
                ProductId.Create(ValidProductId, "HH0001"),
                ProductTitle.Create("Laptop", "Lorem ipsum"),
                ProductPrice.Create(10000000M),
                ProductState.Create(disabled: false, hide: false));

            product.ChangeTitle(ProductTitle.Create("Laptop 14'", "Lorem ipsum 2"));
            product.ChangePrice(ProductPrice.Create(9990000M));

            var mock = new Mock<IEventStore>();
            mock.Setup(x => x.GetEventsOf(ValidProductId)).Returns(product.Events);
            mock.Setup(x => x.GetAllEvents()).Returns(product.Events);

            Repository = new ProductRepository(mock.Object);
        }

        [TestMethod]
        public void When_FindById_Should_ReturnValidProduct()
        {
            var product = Repository.FindById(ValidProductId);

            product.Should().NotBeNull("product should not be null");
            product.Id.Should().Be(ValidProductId, "product id should valid");
            product.Title.Name.Should().Be("Laptop 14'", "product name should be correct");
            product.Title.Description.Should().Be("Lorem ipsum 2", "product description should be correct");
            product.Price.Value.Should().Be(9990000M, "product price should be correct");

            var product2 = Repository.FindById(Guid.NewGuid());

            product2.Should().BeNull("product should be null");
        }
    }
}
