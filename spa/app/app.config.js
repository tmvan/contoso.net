angular.module('contoso')
  .config(routeProviderConfig);

function routeProviderConfig($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'app/sublayouts/home.html'
    })
    .when('/products', {
      templateUrl: 'app/sublayouts/products.html'
    })
    .otherwise({
      redirectTo: '/'
    });
}