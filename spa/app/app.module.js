angular.module('contoso', [
  'ngRoute',
  'contoso.core',
  'contoso.common',
  'contoso.productList',
]);