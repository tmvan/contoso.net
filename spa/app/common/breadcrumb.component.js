(function () {
  'use strict';
  
  angular
  .module ('contoso.common')
  .component ('breadcrumb', {
    template: '<nav aria-label="breadcrumb">'+
    '<ol class="breadcrumb">'+
    '<li class="breadcrumb-item active" aria-current="page">Home</li>'+
    '</ol>'+
    '</nav>'
  });
  
} ());