(function () {
  'use strict';
  
  angular
  .module ('contoso.common')
  .directive ('includeReplace', includeReplace)
  .directive('preventDefault', preventDefaultLink);
  
  function includeReplace() {
    return {
      require: 'ngInclude',
      restrict: 'A',
      link: function (scope, el, attrs) {
        el.replaceWith(el.children());
      }
    }
  }
  
  function preventDefaultLink() {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        if(attrs.href === '' || attrs.href === '#'){
          elem.on('click', function(e){
            e.preventDefault();
          });
        }
      }
    };
  }
  
}());