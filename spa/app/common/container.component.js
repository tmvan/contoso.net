(function () {
    'use strict';

    angular
        .module ('contoso.common')
        .component ('container', {
          transclude: true,
          template: '<div class="container" ng-transclude></div>',
      });


} ());