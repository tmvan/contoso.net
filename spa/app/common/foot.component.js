(function () {
  'use strict';
  
  angular
  .module ('contoso.common')
  .component ('foot', {
    template: '<footer class="py-3 bg-dark text-light">' +
    '<div class="container">' +
    '<small>Designed and built with ❤️ by <strong>@tmvan</strong></small>' +
    '</div>' +
    '</footer>'
  });
  
} ());