(function () {
  'use strict';
  
  angular
  .module ('contoso.common')
  .component ('menubar', {
    templateUrl: 'app/common/menubar.template.html',
  });
  
} ());