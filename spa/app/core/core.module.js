(function(){
  'use strict';
  
  angular
  .module('contoso.core', [
    'contoso.core.product',
    'contoso.core.security'
  ]);
  
}());