(function(){
  'use strict';
  
  angular.module('contoso.core.product')
  .factory('productService', productService);
  
  productService.$inject = ['$http', '$q', 'apiHelper'];
  function productService($http, $q, apiHelper) {
    this.getAll = function(page) {
      var deferred = $q.defer();
      var productApiUrl = apiHelper.productApi;
      
      $http({
        method: 'GET',
        url: productApiUrl,
        params: {pageIndex: page, pageSize: 100}
      }).then(function(resp) {
        deferred.resolve(resp.data);
      }).catch(function(resp) {
        deferred.reject(new Error(resp.data.error_description || resp.data.error));
      });
      
      return deferred.promise;
    };
    
    return this;
  }
  
}());