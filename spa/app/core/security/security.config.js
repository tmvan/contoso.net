(function(){
  'use strict';

  angular.module('contoso.core.security')
  .config(localStorageConfigure);
  
  function localStorageConfigure(localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('contoso.');
  }
  
}());