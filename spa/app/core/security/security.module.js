(function(){
  'use strict';
  
  angular.module('contoso.core.security', [
    'LocalStorageModule'
  ]);
  
}());