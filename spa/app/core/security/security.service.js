(function(){
  'use strict';
  
  angular.module('contoso.core.security')
  .factory('apiHelper', apiHelper)
  .factory('authService', authService);
  
  apiHelper.$inject = [];
  function apiHelper() {
    var baseUrl = 'http://api.contoso.local/';
    return {
      baseUrl: baseUrl,
      issueTokenApi: baseUrl + 'oauth/token',
      productApi: baseUrl + 'api/v1/products',
      clientId: 'client',
      clientSecret: 'secret'
    };
  }
  
  authService.$inject = ['$http', '$q', 'localStorageService', 'apiHelper'];
  function authService($http, $q, localStorageService, apiHelper) {
    this.isLoggedIn = function() {
      return localStorageService.get('token') != null;
    }
    
    this.login = function(userName, password) {
      var defered = $q.defer();
      
      if (!this.isLoggedIn()) {
        acquireToken(userName, password)
        .then(function(data) {
          localStorageService.set('token', data); 
          defered.resolve();
        })
        .catch(function(err) {
          defered.reject(err);
        });
      } else {
        defered.resolve();
      }
      
      return defered.promise;
    }
    
    this.logout = function() {
      localStorageService.remove('token');
    }
    
    function acquireToken(userName, password) {
      var deferred = $q.defer();
      var tokenIssuerApiUrl = apiHelper.issueTokenApi;
      var data = {
        grant_type: 'password',
        client_id: apiHelper.clientId,
        client_secret: apiHelper.clientSecret,
        username: userName,
        password: password
      };
      
      $http({
        method: 'POST',
        url: tokenIssuerApiUrl,
        data: window.jQuery.param(data),
        headers: { 
          'Content-Type': 'application/x-www-form-urlencoded' 
        }
      }).then(function(resp) {
        deferred.resolve(resp.data);
      }).catch(function(resp) {
        deferred.reject(new Error(resp.data.error_description || resp.data.error));
      });
      
      return deferred.promise;
    }
    
    return this;
  }
  
}());