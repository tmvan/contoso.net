(function () {
  'use strict';
  
  angular
  .module ('contoso.productList')
  .component ('productList', productList());
  
  
  function productList() {
    
    productListController.$inject = ['productService'];
    function productListController(productService) {
      var vm = this;
      
      vm.products = [];
      vm.currentPage = 1;
      vm.totalPages = 0;
      vm.totalResults = 0;
      
      init();
      
      function init() {
        
        productService.getAll(1)
        .then(function(data) {
          vm.products = data.products;
          vm.totalPages = data.totalPages;
          vm.totalResults = data.totalResults;
        })
        .catch(function(err) {
          console.error(err);
          alert(err.message);
        });
        
      }
    }
    
    return {
      templateUrl: 'app/product-list/product-list.template.html',
      controller: productListController,
    }
  }
  
} ());