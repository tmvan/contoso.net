(function(){
  'use strict';
  
  angular
  .module('contoso.productList', [
    'contoso.core.product'
  ]);
  
}());